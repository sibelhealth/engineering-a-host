from distutils.core import setup
import py2exe

setup(
    options = {
     "py2exe": {
        "dll_excludes": ["MSVCP90.dll"],
        "includes": ["urlparse","email.mime.*","packaging","packaging.*","pkg_resources._vendor.appdirs", 'scipy', 'scipy.integrate', 'scipy.special.*','scipy.linalg.*','scipy.sparse.csgraph._validation','scipy._lib.messagestream','pywt._extensions._cwt']
    }},console=['nicu.py'])