"""=================================================================
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
====================================================================
"""

__author__ = "Jong Yoon Lee"
__license__ = "MIT"
__version__ = "1"
__email__ = "jlee642@illinois.edu"

import logging
import Queue
import sys
import time
from multiprocessing import Process
from threading import Lock, Thread, Timer

from data_logger import DataLogger
import numpy as np
from pc_ble_driver_py import exceptions as e
from pc_ble_driver_py import config
from pc_ble_driver_py.observers import *
import ctypes as c


from comm.event_communicator import *
from comm.graph_communicator import GraphCommunicator

CONNECTIONS = 2

class PeriodicTask(object):
    def __init__(self, interval, callback, daemon=True, **kwargs):
        self.interval = interval
        self.callback = callback
        self.daemon   = daemon
        self.kwargs   = kwargs

    def run(self):
        self.callback(**self.kwargs)
        t = Timer(self.interval, self.run)
        t.daemon = self.daemon
        t.start()


class NICUCollect(BLEDriverObserver, BLEAdapterObserver):

    COMPANY_ID = [255,255]

    ECG_BUFFER_LEN = 48
    ECG_NUM_TYPE = 1
    ECG_NUM_DATA = ECG_BUFFER_LEN / ECG_NUM_TYPE/2

    EMG_BUFFER_LEN = 48
    EMG_NUM_TYPE = 2
    EMG_NUM_DATA = EMG_BUFFER_LEN / EMG_NUM_TYPE/3

    def __init__(self, event_comm, graph_comm, test, baud_rate=1000000):

        super(NICUCollect, self).__init__()
        self.conn_q  = Queue.Queue()

        self.ecg_buf = np.zeros( self.ECG_NUM_DATA, dtype=np.float)
        self.emg1_buf = np.zeros( self.EMG_NUM_DATA, dtype=np.float)
        self.emg2_buf = np.zeros( self.EMG_NUM_DATA, dtype=np.float)


        self.event_comm = event_comm
        self.graph_comm = graph_comm
        self.test = test

        self.connection_thread = None

        #initalize Data Logger
        self.data_logger = DataLogger()

        self.master_handle = -1
        self.slave_handle = -1

        self.dev = 0
        self.num = 0

        self.connection_tuple = None
        self.connection_tuple_lock = Lock()

        self.conn_q.put(-1)
        self.advertising = False
        self.ca_mode = False

        """
        Below are user function callback, we attach these callbacks to the event communicator. The callbacks,
        within the NICU BLE driver's process, when the user interacts with the GUI frontend in such a way that
        it sends a message to the event comm which is then handled by the BLE driver's event comm and invokes 
        one of the correct functions below.
        """
        self.event_comm.add_event(BLE_EVENT.STARTADVERTISE, self.launch_advertise_thread)
        self.event_comm.add_event(BLE_EVENT.STOPADVERTISE, self.stop_advertise_thread)
        self.event_comm.add_event(BLE_EVENT.CONNECT, self.set_connection_tuple)
        self.event_comm.add_event(BLE_EVENT.DISCONECT, self.handle_user_disconnect)
        self.event_comm.add_event(BLE_EVENT.EXIT, self.handle_user_exit)
        self.event_comm.add_event(BLE_EVENT.STARTLOGGING, self.start_logging)
        self.event_comm.add_event(BLE_EVENT.STOPLOGGING, self.stop_logging)
        self.event_comm.add_event(BLE_EVENT.MODE_CHANGE, self.change_mode)

        # Start the bluetooth driver or start the simulated signal
        try:
            self.set_connectivity_id("NRF52")
            if self.test == True:
                self.test_bt()
            else:
                ret_val = self.setup_bluetooth_driver(baud_rate)
                if ret_val == False:
                    self.event_comm.post_event(QT_EVENT.EXIT, ())
                else:
                    self.adapter.observer_register(self)
                    self.adapter.driver.observer_register(self)
                    self.event_comm.post_event(QT_EVENT.BT_CONNECTED, (True,))
        except Exception as e:
            log.error("Error While initalizing the BLE Dongle with {}", e)
            self.event_comm.post_event(QT_EVENT.EXIT, ())

    def test_bt(self):

        # Sin Function
        Fs = 500
        x = np.arange(Fs)
        self.sim_sin = (10000 * np.sin(2* np.pi * x/500) + 10000).astype(int)
        self.sim_sin = (np.ones(500) * 500).astype(int)
        self.sim_sin[200] = 1000
        # self.sim_sin[202] = 1000
        # for i in range(200):
        #     self.sim_sin[i] = 0
        #     self.sim_sin[500-i] = 0

        
        self.master_handle = 1
        self.slave_handle = 2

        self.sim_m_counter = 0
        self.sim_s_counter = 250
        task = PeriodicTask(interval=.01, callback=self.stream_simulation_data)

        task.run()

    def stream_simulation_data(self):
        class ecg(object):
            value =  0x2a18
        class temp(object):
            value =  0x2A1C
        class ppg(object):
            value =  0x2A49
        class scg(object):
            value =  BLEUUID.Standard.heart_rate

        ecg = ecg()
        temp = temp()
        ppg = ppg()
        scg = scg()

        # Simulate ECG Data being sent every 20 ms
        if self.sim_m_counter % 10 == 0:
            m_data = self.sim_sin[self.sim_m_counter: self.sim_m_counter+10]
            m_data_bytes = []
            for i in range(0,10):
                m_data_bytes.append(m_data[i] | 0xff)
                m_data_bytes.append(m_data[i] >> 8)

            self.on_notification(None, self.master_handle, ecg, m_data_bytes)

        # Simulate ECG Data being sent every 50 ms
        if self.sim_s_counter % 25 == 0:

            s_data = self.sim_sin[self.sim_s_counter: self.sim_s_counter+25]

            s_data_bytes = []
            for i in range(0,5):
                s_data_bytes.append(s_data[i * 5] >> 8)
                s_data_bytes.append(s_data[i * 5] | 0xff)
                s_data_bytes.append(s_data[i * 5] >> 8)
                s_data_bytes.append(s_data[i * 5] | 0xff)

            self.on_notification(None, self.slave_handle, ppg, s_data_bytes)

        self.sim_m_counter += 5
        self.sim_s_counter += 5

        if self.sim_m_counter == 500:
            self.sim_m_counter = 0
        if self.sim_s_counter == 500:
            self.sim_s_counter = 0

    def start_logging(self, args):
        self.data_logger.open(self.ca_mode)
    
    def stop_logging(self, args):
        self.data_logger.close()
    def change_mode(self, args):
        logging.info("args for change mode: {}".format(args[0]))
        self.ca_mode = args[0]

    """
    This function is triggered when the user requests to disconnect from the current device. 
    The disconnect_num is the device with which we wish to disconnect from.
    """
    def handle_user_disconnect(self, disconnect_tuple):
        disconnect_num = disconnect_tuple[0]

        if disconnect_num == 0:
            self.adapter.disconnect(self.master_handle)
            self.master_handle = -1
        else:
            self.adapter.disconnect(self.slave_handle)
            self.slave_handle = -1

    """
    This function is triggered when the user exits the program. The BLE dongle connection must be closed 
    otherwise a manual turn off and then back on is required in order to get a BLE device to connect to 
    the dongle.
    """
    def handle_user_exit(self, args):
        if self.test != True:
            try:
                self.adapter.driver.close()
                logging.info("Done handling user exit")
            except:
                pass
        logging.info("Handling user exit in nicu_bt")
        self.data_logger.stop_data_loop()

    """
    This function is triggered on instantiation of the class, and prepares the BLE dongle for the running
    of the program. The number of connection is how many BLE devices we wish to connect to the dongle.
    """   
    def open(self):
        logging.info("Opening Device")
        try:
            self.adapter.driver.open()
        except Exception as e:
            self.event_comm.post_event(QT_EVENT.EXIT, ())
            logging.exception("Error opening the Driver")
            return

        ble_enable_params = BLEEnableParams(vs_uuid_count      = 1,
                                            service_changed    = False,
                                            periph_conn_count  = 0,
                                            central_conn_count = CONNECTIONS,
                                            central_sec_count  = CONNECTIONS)
        if nrf_sd_ble_api_ver >= 3:
            logging.info("Enabling larger ATT MTUs")
            ble_enable_params.att_mtu = 200
        try:
            self.adapter.driver.ble_enable(ble_enable_params)
        except Exception as e:
            self.event_comm.post_event(QT_EVENT.EXIT, ())
            logging.exception("Error Enabling BLE")
            return

    def close(self):
        self.adapter.driver.close()


    """
    This is the starting point for the BLE connection process. 
    """
    def connect_and_discover(self):
        #While we have not connected to anything loop forever until either we connect to the desired device or
        #until the user exits the connection process.
        while self.master_handle == -1 or self.slave_handle == -1:
            try:
                self.adapter.driver.ble_gap_scan_start()
            except Exception as e:
                logging.warning("Connect and Discover Exception: %s", e)
                return

            try:
                #Wait until we have selected a device to connect to, this will return either after a timeout or we have 
                #selected a device to connect to. This block and is dependent upon on_gap_evt_adv_report to select a 
                #connection handle from the various devices available.
                new_conn = self.conn_q.get(timeout = 180)
            except Exception as e:
                logging.debug("conn queue threw error %s", e)
                return

            if new_conn == -1:
                return

            if nrf_sd_ble_api_ver >= 3:
                try:
                    att_mtu = self.adapter.att_mtu_exchange(new_conn)
                except KeyError, e:
                    logging.warning("MTU exchange failed: %s", e)
                    # device_type, _name = self.connection_tuple
                    #This resets the GUI's button to NOT connected.
                    # self.event_comm.post_event(QT_EVENT.UPDATE_BUTTON_STATUS, (device_type,0,))
                    continue

            try:
                #After we have connected with a device we request, from the connected device, what are the various 
                #services that you offer, currently the callback for this is NOT implemented.
                self.adapter.service_discovery(new_conn)
            except Exception as e:
                logging.warning("adapter service discovery failed: %s", e)
                # with self.connection_tuple_lock:
                #     device_type, _name = self.connection_tuple
                #     #This resets the GUI's button to NOT connected.
                #     self.event_comm.post_event(QT_EVENT.UPDATE_BUTTON_STATUS, (device_type,0,))
                continue

            with self.connection_tuple_lock:
                device_type, _name = self.connection_tuple
                #This sets the GUI's button to connected.
                self.event_comm.post_event(QT_EVENT.UPDATE_BUTTON_STATUS, (device_type,2,))

                if device_type == 0:
                    self.master_handle = new_conn
                else:
                    self.slave_handle = new_conn

                self.connection_tuple = None
            logging.info("Device connected")

        self.advertising = False

    """
    This method connects to the passed in handle. The handle that is passed comes from the
    self.adapter.connect call in the on_gap_evt_adv_report method.
    """ 
    def on_gap_evt_connected(self, ble_driver, conn_handle, peer_addr, role, conn_params):
        logging.info('New Connection: {}'.format(conn_handle))
        self.conn_q.put(conn_handle)

    """

    """
    def on_gap_evt_disconnected(self, ble_driver, conn_handle, reason):
        logging.info('Disconnected: {} {} {}'.format(conn_handle, reason, reason.value))

        # time out or failed to be established
        if reason.value == 8:
            if(conn_handle == self.master_handle):
                self.event_comm.post_event(QT_EVENT.DISCONNECT, (0,))
                self.master_handle = -1
            elif conn_handle == self.slave_handle:
                self.event_comm.post_event(QT_EVENT.DISCONNECT, (1,))
                self.slave_handle = -1
            else:
                logging.info("Device disconnected while trying to connect")
                with self.connection_tuple_lock:
                    device_type, _name = self.connection_tuple
                    self.event_comm.post_event(QT_EVENT.DISCONNECT, (device_type,))
            with self.connection_tuple_lock:
                self.connection_tuple = None
        if reason.value == 62:
            device_type, _name = self.connection_tuple
            logging.info("Disconnect with failed to be established Device type: {}".format(device_type))

            if device_type == 0:
                self.event_comm.post_event(QT_EVENT.DISCONNECT, (0,))
            else:
                self.event_comm.post_event(QT_EVENT.DISCONNECT, (1,))
            with self.connection_tuple_lock:
                self.connection_tuple = None
    def on_gap_evt_timeout(self, ble_driver, conn_handle, src):
        if src == BLEGapTimeoutSrc.scan:
            ble_driver.ble_gap_scan_start()

    """
    """
    def on_gap_evt_adv_report(self, ble_driver, conn_handle, peer_addr, rssi, adv_type, adv_data):
        dev_name_list = None
        if BLEAdvData.Types.complete_local_name in adv_data.records:
            dev_name_list = adv_data.records[BLEAdvData.Types.complete_local_name]
        elif BLEAdvData.Types.short_local_name in adv_data.records:
            dev_name_list = adv_data.records[BLEAdvData.Types.short_local_name]
        else:
            return
        # Check if the ncessary manufactuer data exist
        if BLEAdvData.Types.manufacturer_specific_data in adv_data.records:
            print(adv_data.records[BLEAdvData.Types.manufacturer_specific_data])
            if self.COMPANY_ID != adv_data.records[BLEAdvData.Types.manufacturer_specific_data][:2]:
                return
        else:
            return

        # Get device type 0 - chest 1 - limb
        device_type = adv_data.records[BLEAdvData.Types.manufacturer_specific_data][2]

        dev_name        = "".join(chr(e) for e in dev_name_list)
        address_string  = "".join("{0:02X}".format(b) for b in peer_addr.addr)
        logging.info('Received advertisment report, address: 0x{}, device_name: {}'.format(address_string,
                                                                                    dev_name))
        if dev_name not in self.master_list and device_type == 5:
            logging.info('Added {} to master list'.format(dev_name))
            self.event_comm.post_event(QT_EVENT.UPDATE_M_LIST, (dev_name))
            self.master_list.append(dev_name)

        if dev_name not in self.slave_list and device_type == 1:
            logging.info('Added {} to slave list'.format(dev_name))
            self.event_comm.post_event(QT_EVENT.UPDATE_S_LIST, (dev_name))
            self.slave_list.append(dev_name)

        with self.connection_tuple_lock:
            if not self.connection_tuple:
                return

            device_type, name = self.connection_tuple
            if device_type == 0 and name == dev_name:
                logging.info('The selected master is: %s', dev_name)
                self.adapter.connect(peer_addr)

            if device_type == 1 and name == dev_name:
                logging.info('The selected slave is: %s', dev_name)
                self.adapter.connect(peer_addr)

    def on_notification(self, ble_adapter, conn_handle, uuid, data):
        try:
            if self.advertising == False:
                if conn_handle == self.master_handle:

                    # ECG
                    if uuid.value == 0x2A18:
                        for i in range(self.ECG_NUM_DATA):
                            ecg = (data[2*i+1]) | data[2*i]  << 8
                            self.ecg_buf[i] = ecg
                        ts_ms = (data[self.ECG_BUFFER_LEN + 3] << 24) | (data[self.ECG_BUFFER_LEN + 2] << 16) | (data[self.ECG_BUFFER_LEN + 1] << 8) | data[self.ECG_BUFFER_LEN]
                        ts_ms = ts_ms / 4.
                        # self.ecg_queue.put(self.ecg_buf)

                        self.graph_comm.update_graph( 1, (self.ecg_buf,self.ECG_NUM_DATA))             
                        self.data_logger.write_ecg(ts_ms, self.ecg_buf, self.ECG_NUM_DATA)

                    # ACCELOMETER
                    elif uuid.value == BLEUUID.Standard.heart_rate:
                        if self.ca_mode == False:
                            for i in range(self.EMG_NUM_DATA):
                                self.emg1_buf[i] = c.c_int32(data[i * 6] << 24 | data[i * 6 + 1] << 16 | data[i * 6 + 2] << 8).value  >> 8
                                self.emg2_buf[i] = c.c_int32(data[i * 6 + 3] << 24 | data[i * 6 + 4] << 16 | data[i * 6 + 5] << 8).value  >> 8

                            ts_ms = (data[self.EMG_BUFFER_LEN + 3] << 24) | (data[self.EMG_BUFFER_LEN + 2] << 16) | (data[self.EMG_BUFFER_LEN + 1] << 8) | data[self.EMG_BUFFER_LEN]
                            ts_ms = ts_ms / 4.

                            self.graph_comm.update_graph( 2, (self.emg1_buf,self.EMG_NUM_DATA))             
                            self.graph_comm.update_graph( 3, (self.emg2_buf,self.EMG_NUM_DATA))             

                            self.data_logger.write_emg0(ts_ms, self.emg1_buf, self.EMG_NUM_DATA)
                            self.data_logger.write_emg1(ts_ms, self.emg2_buf, self.EMG_NUM_DATA)

                            # self.scg_queue.put(self.z_buf)
                            # self.data_logger.write_accl(ts_ms, self.x_buf, self.y_buf, self.z_buf, self.ACCL_NUM_DATA)

        except Exception as e: print(e)

    def set_connectivity_id(self, connectivity_device):
        global BLEDriver, BLEAdvData, BLEEvtID, BLEAdapter, BLEEnableParams, BLEGapTimeoutSrc, BLEUUID
        config.__conn_ic_id__ = connectivity_device

        from pc_ble_driver_py.ble_driver    import BLEDriver, BLEAdvData, BLEEvtID, BLEEnableParams, BLEGapTimeoutSrc, BLEUUID
        from pc_ble_driver_py.ble_adapter   import BLEAdapter

        global nrf_sd_ble_api_ver
        nrf_sd_ble_api_ver = config.sd_api_ver_get()
        self.nrf_sd_ble_api_ver = config.sd_api_ver_get()

    def launch_advertise_thread(self, args):
        if not self.connection_thread or not self.connection_thread.isAlive():
            with self.conn_q.mutex:
                self.conn_q.queue.clear()
            self.master_list = []
            self.slave_list = []

            
            self.advertising = True
            self.connection_thread = Thread(target=self.connect_and_discover)
            self.connection_thread.start()

    def on_gattc_evt_exchange_mtu_rsp(self, ble_driver, conn_handle, **kwargs):
        logging.info('ATT MTU exchange response: conn_handle={}'.format(conn_handle))    

    def on_att_mtu_exchanged(self, ble_driver, conn_handle, att_mtu):
        logging.info('ATT MTU exchanged: conn_handle={} att_mtu={}'.format(conn_handle, att_mtu))

    def set_connection_tuple(self, connection_tuple):
        with self.connection_tuple_lock:
            self.connection_tuple = connection_tuple

    def setup_bluetooth_driver(self, baud_rate):
        descs       = BLEDriver.enum_serial_ports()
        if len(descs) == 0:
            logging.info('BLE Dongle not found')
            return False
        serial_port = descs[0].port
        logging.info("Connecting to {}".format(serial_port))
        driver    = BLEDriver(serial_port=serial_port, baud_rate=baud_rate, auto_flash=False)
        self.adapter   = BLEAdapter(driver)
        self.open()
        return True

    def stop_advertise_thread(self, args):
        if self.connection_thread.isAlive():
            try:
                self.adapter.driver.ble_gap_scan_stop()
            except:
                self.event_comm.post_event(QT_EVENT.EXIT, ())
                logging.exception("Error opening the Driver")
                return

            self.conn_q.put(-1)
            self.advertising = False

        start_time = time.time()
        try:
            if self.master_handle != -1:
                self.adapter.enable_notification(self.master_handle, BLEUUID(0x2A18)) # ECG
                self.adapter.enable_notification(self.master_handle, BLEUUID(BLEUUID.Standard.heart_rate)) # EMG
            if self.slave_handle != -1:
                self.adapter.enable_notification(self.master_handle, BLEUUID(0x2A18)) # ECG
                self.adapter.enable_notification(self.master_handle, BLEUUID(BLEUUID.Standard.heart_rate)) # EMG
        except:
            # log.debug(" characistic notification enable failed")
            return

    def on_att_mtu_exchanged(self, ble_driver, conn_handle, att_mtu):
        logging.info('ATT MTU exchanged: conn_handle={} att_mtu={}'.format(conn_handle, att_mtu))

    def run(self):
        self.event_comm.start_event_handler(PROCESS.BLUETOOTH)

def launch(event_comm, graph_comm ,test, mac):
    if mac == True:
        BT = NICUCollect(event_comm, graph_comm, test, 115200)
    else:
        BT = NICUCollect(event_comm, graph_comm, test)

    BT.run()

def fork_bt_process(event_comm, graph_comm, test, mac):
    bt_process = Process(target=launch, args=(event_comm, graph_comm, test, mac))
    return bt_process

def main():

    # Intializeing communicators
    event_comm = EventCommunicator()
    graph_comm = GraphCommunicator()

    ble_process = Process(target=launch, args=(event_comm, graph_comm,))
    ble_process.start()

    while True:
        pass

if __name__ == "__main__":
    main()
