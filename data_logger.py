"""=================================================================
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
====================================================================
"""

__author__ = "Jong Yoon Lee"
__license__ = "Apache"
__version__ = "0.1.2"
__email__ = "jlee642@illinois.edu"

from time import localtime, strftime, time
import numpy as np
import os
import json
from threading import Timer, Lock
from pyqtgraph.Qt import QtCore
import logging
from threading import Lock, Thread, Timer
import Queue

class DataLogger(object):

    fileName = ""
    objectOpen = 0

    VERSION = "0.0.1"
    ECG_SAM_INTERVAL = 1.98364257
    EMG_SAM_INTERVAL = 2

    def __init__(self):

        self.data_write_queue = Queue.Queue()
        self.dl_thread = Thread(target= self.write_data_loop)
        self.dl_thread.start()

        logging.debug("Data logger Initialized: %s", self)

        # Populate Info Dict
        self.info = {}
        self.info['version'] = self.VERSION
        self.info['ecg_sampling'] = self.ECG_SAM_INTERVAL
        self.info['emg_sampling'] = self.EMG_SAM_INTERVAL

    def write_data_loop(self):
        try:
            while True:
                dl_file, data = self.data_write_queue.get()
                if len(data) == 0:
                    return
                dl_file.write(data)

        except Exception as e: print(e)
    def stop_data_loop(self):
        self.data_write_queue.put((None, []))
        self.dl_thread.join()
            
    def open(self, ca_mode):
        self.ca_mode = ca_mode
        if (self.objectOpen == 0):
            logging.debug("Opened logger")

            desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
            if not os.path.exists(desktop+"\\log_file"):
                os.makedirs(desktop+"\\log_file")

            # Create necessary folders
            self.folderName =desktop+"\\log_file\\doppler_" + strftime("%Y-%m-%d-%H-%M-%S", localtime())
            os.makedirs(self.folderName)

            self.ecg_file = open(self.folderName + "\\ecg.tsv", "w")
            self.emg0_file = open(self.folderName + "\\emg0.tsv", "w")
            self.emg1_file = open(self.folderName + "\\emg1.tsv", "w")

            self.ecg_file.write("time\tecg\n")
            self.emg0_file.write("time\temg\n")
            self.emg1_file.write("time\temg\n")

            # Write Info file
            self.info['start_time_epoch'] = time()
            with open(self.folderName + "\\info.json", 'w') as outfile:
                json.dump(self.info, outfile, indent=4, separators=(',', ': '))
            
            self.objectOpen = 1

    def write_ecg(self, ts_ms, ecg_buffer,b_len):
        if (self.objectOpen == 1):
            data = ""
            try:
                for i in range(b_len):
                    data += str(ts_ms - self.ECG_SAM_INTERVAL * (b_len - 1) + i * self.ECG_SAM_INTERVAL) + "\t" + str(ecg_buffer[i]) +"\n"
                self.data_write_queue.put((self.ecg_file, data))
            except Exception as e:
                print(e)


    def write_emg0(self, ts_ms, emg_buffer,b_len):
        if (self.objectOpen == 1):
            data = ""
            for i in range(b_len):
                data += str(ts_ms - self.EMG_SAM_INTERVAL * (b_len - 1) + i * self.EMG_SAM_INTERVAL) + "\t" + str(emg_buffer[i]) +"\n"
            self.data_write_queue.put((self.emg0_file, data))

    def write_emg1(self, ts_ms, emg_buffer,b_len):
        if (self.objectOpen == 1):
            data = ""
            for i in range(b_len):
                data += str(ts_ms - self.EMG_SAM_INTERVAL * (b_len - 1) + i * self.EMG_SAM_INTERVAL) + "\t" + str(emg_buffer[i]) +"\n"
            self.data_write_queue.put((self.emg1_file, data))

    def close(self):
        self.objectOpen = 0
        self.ecg_file.close()
        self.emg0_file.close()
        self.emg1_file.close()

        logging.debug("Closed logger object")

def main():
    log = DataLogger()
    log.open()
    # log.write_ecg([i for i in range(10)])
    # log.write_ppg([i for i in range(5)],[i for i in range(5)])
    # log.write_accl([i for i in range(3)],[i for i in range(3)],[i for i in range(3)])
    # log.write_chest_temp(5)
    # log.write_foot_temp(10)
    while True:
        pass
    log.close()
    # log.write(3, 4, 5, 6, 7)
    # log.close()

if __name__ == '__main__':
    main()   
