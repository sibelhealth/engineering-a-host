from multiprocessing import Pipe
from threading import Lock, Thread, Timer

class SP_WRAPPER():

    def __init__(self, sp_agent):
        pass
        self.sp_s_conn, self.sp_e_conn = Pipe()
        self.bt_s_conn, self.bt_e_conn = Pipe()

        # Initalize events to None
        self.signal_processing_agent = sp_agent
    def start(self):

        self.signal_processing_agent.listen()
        while True:
            try:
                data = self.sp_e_conn.recv()
                if len(data) == 0:
                    self.bt_s_conn.send(-10001)
                    return

                # Feed data to the signal processing agent and check if new value is avaliable
                value_update = self.signal_processing_agent.feed_data(data)

                # Call callback if updated
                if value_update:
                    self.bt_s_conn.send(self.signal_processing_agent.get_value())

            except EOFError:
                print("EOF Error")
                break
    def feed_data(self, vs_value):
        self.sp_s_conn.send(vs_value)

    def listen_vital_signal_updates(self, callback):
        #I can't pickle the lock this is not safe.
        while True:
            try:
                obj = self.bt_e_conn.recv()
                # magic number of ending the loop
                if obj == -10001:
                    return
                callback(obj)
            except EOFError:
                print("EOF Error")
                break