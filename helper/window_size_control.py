"""=================================================================
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
====================================================================
"""

__author__ = "Jong Yoon Lee"
__license__ = "Apache"
__version__ = "0.1.0"
__email__ = "jlee642@illinois.edu"

class window_size_control(object):
    """This class controls the size of the objects relative to screen Resolution"""

    def __init__(self, geometry):
        """Gets the geometry object and obtains the windows height, width"""

        self.width = geometry.width()
        self.height = geometry.height()

    def mainHeight(self):
        return self.height
    def mainWidth(self):
        return self.width
    def fontSize(self):
        return max(30, self.width/50)
    def titlefontSize(self):
        return max(30, self.width/35)
    def marginWidth(self):
        return max(30, self.width/70)
    def marginHeight(self):
        return max(30, self.height/40)
    def Border(self):
        pass
    def graphHeight(self):
        return self.height * .9 - self.marginHeight()
    def graphWidth(self):
        return self.width * .7

    ##BIOInfo related Sizes##
    def bioInfoHeight(self):
        return self.height * .7 - self.marginHeight()
    def bioInfoWidth(self):
        return self.width * .25
    def bioInfoLeft(self):
        return self.width - self.width * .25 - self.marginWidth()
    def bioInfoTop(self):
        return self.height/ 10

    #BIOInfotext
    def bioInfotextsize(self):
        return self.height/7
    def Temptextsize(self):
        return self.height/12
    def bioInfonametextsize(self):
        return self.height/35
    def bioInfonametextTempsize(self):
        return self.height/45
        
    #Buttontext
    def buttonTop(self):
        return self.bioInfoTop()+ self.bioInfoHeight()
    def buttonInfoHeight(self):
        return self.height - self.buttonTop() - self.marginHeight()


    #ConnectWidget
    def connectBTWIdth(self):
        return self.width * .50
    def connectBTHeight(self):
        return self.height/2.5
    def connectfontsize(self):
        return self.connectBTHeight()/25

    ##Clock##
    def clockHeight(self):
        return self.titlefontSize() *1.4
    def clockWidth(self):
        return self.width*.14
    def clockLeft(self):
        return self.bioInfoLeft() -self.width*2/10
    def clockTop(self):
        return self.marginHeight()