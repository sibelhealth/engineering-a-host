"""=================================================================
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
====================================================================
"""

__author__ = "Jong Yoon Lee"
__license__ = "Apache"
__version__ = "0.1.0"
__email__ = "jlee642@illinois.edu"

import pyqtgraph as pg
import numpy as np
from threading  import Lock
import logging

class Graph(object):

    def __init__(self,parent, name,color, viewrange, metric=False, m_axis = False, range = [30,400]):
        self.graphobject = pg.PlotWidget(parent,title="<text style='color:"+color+";font-size: 40px;'>"+name+"</text>")
        # pg.setConfigOption('rightButtonPan', False)
        # self.pg.ViewBox.setMouseEnabled(x= False, y = False)
        self.graphobject.setXRange(0, viewrange, 0)
        # self.graphobject.setXRange(500, viewrange-300)
        self.metric = metric
        self.m_axis = m_axis
        self.graphobject.hideButtons()


        if metric is True:
            self.graphobject.setYRange(range[0], range[1])
            self.curve = (self.graphobject.plot(pen=color, connect="finite"))
            self.buffer = np.empty(viewrange)
            self.buffer.fill(np.nan)
            self.buffer[0] = 30 
            xAxis = self.graphobject.getAxis('bottom')
            xAxis.setStyle(showValues = False)
            xAxis = self.graphobject.getAxis('left')
        elif m_axis is True:
            # self.graphobject.setYRange(-65536, 65536, 0)
            # self.graphobject.addLegend(size=(30,30), offset=(0, 30))

            # self.curve1 = self.graphobject.plot(pen=(0,140,186), name="<font size='3'>X</font>")
            # self.curve2 = self.graphobject.plot(pen=(82,190,128), name="<font size='3'>Y</font>")
            self.curve3 = self.graphobject.plot(pen=(240,128,128),  name="<font size='3'>Z</font>")
            self.Xbuffer = np.empty(0, dtype=np.int)
            self.Ybuffer = np.empty(0, dtype=np.int)
            self.Zbuffer = np.empty(0, dtype=np.int)
            xAxis = self.graphobject.getAxis('bottom')
            xAxis.setStyle(showValues = False)
            # yAxis =  self.graphobject.getAxis('left')
            # yAxis.setStyle(showValues = False)

        else:
            # self.graphobject.setYRange(0, 16384)
            self.curve = (self.graphobject.plot(pen=color, connect="finite"))
            # self.curve.setMouseEnabled(y=False)
            self.buffer = np.empty(viewrange)
            self.buffer.fill(np.nan)
            self.buffer[0] = 0
            xAxis = self.graphobject.getAxis('bottom')
            xAxis.setStyle(showValues = False)
            # yAxis =  self.graphobject.getAxis('left')
            # yAxis.setStyle(showValues = False)

        self.range = viewrange
        self.cur_pos = 0

        self.space_len = self.range/100

#self.graph_1.plot(pen=(93,173,226)
    def getGraph(self):
        """ returns the graph object generated by the class"""
        return self.graphobject

    def getData(self):
        """get the data used for graphing"""
        buffer =  self.buffer
        return buffer

    def updateData(self, data, data_len):

        for i in range(data_len):
            new_insert_pos = (self.cur_pos + i)
            if new_insert_pos >= self.range:
                break
            # self.buffer[new_insert_pos] = data[i]
            self.buffer[new_insert_pos] = data[i]

        new_insert_pos += 1

        for i in range(self.space_len):
            if new_insert_pos + i >= self.range:
                break
            self.buffer[(new_insert_pos + i)] = np.nan
    
        self.cur_pos = new_insert_pos 
            #self.curve.setData(self.buffer)

    def updateMetric(self, data, data_len):
        for i in range(data_len):
            new_insert_pos = (self.cur_pos + i)
            if new_insert_pos >= self.range:
                break
            if data[i] == -1:
                self.buffer[new_insert_pos] = np.nan 
            else:
                self.buffer[new_insert_pos] = data[i]

        new_insert_pos += 1

        for i in range(self.space_len):
            if new_insert_pos + i >= self.range:
                break
            self.buffer[(new_insert_pos + i)] = np.nan
    
        self.cur_pos = new_insert_pos 
        if self.cur_pos == self.range:
            self.cur_pos = 0
    def update3axis(self, data, data_len):
        if self.Zbuffer.shape[0] < self.range:
            # self.Xbuffer = np.append(self.Xbuffer, data[0])
            # self.Ybuffer = np.append(self.Ybuffer, data[1])
            self.Zbuffer = np.append(self.Zbuffer, data)
        else:
            # self.Xbuffer = np.concatenate((self.Xbuffer[data_len:], data[0]))
            # self.Ybuffer = np.concatenate((self.Ybuffer[data_len:], data[1]))
            self.Zbuffer = np.concatenate((self.Zbuffer[3:], data))
    def clear_graph(self):
        if self.m_axis:
            self.Xbuffer = np.empty(self.range)
            self.Xbuffer.fill(np.nan)
            self.Ybuffer = np.empty(self.range)
            self.Ybuffer.fill(np.nan)
            self.Zbuffer = np.empty(self.range)
            self.Zbuffer.fill(np.nan)
        else:
            self.buffer = np.empty(self.range)
            self.buffer.fill(np.nan)
        self.graphobject.setXRange(0, self.range,0)
        self.cur_pos = 0
        logging.debug("graph cleared")