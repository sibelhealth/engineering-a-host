"""=================================================================
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
====================================================================
"""

__author__ = "Jong Yoon Lee"
__license__ = "Apache"
__version__ = "0.1.0"
__email__ = "jlee642@illinois.edu"

# -*- coding: utf-8 -*-

import logging
import sys, time
from multiprocessing import Process
from threading import Thread

import numpy as np
# Form implementation generated from reading ui file 'prosthetic_gui.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!
import pyqtgraph as pg
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from connectWindow import ConnectWindow
from comm.event_communicator import *
from graph import Graph
from comm.graph_communicator import GraphCommunicator
from helper.setstylesheet import *
from helper.window_size_control import window_size_control

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class NICU_GUI(QtGui.QMainWindow):

    hr_update_signal = QtCore.pyqtSignal(tuple)
    spo2_update_signal = QtCore.pyqtSignal(tuple)
    temp_update_signal = QtCore.pyqtSignal(tuple)
    ptt_update_signal = QtCore.pyqtSignal(tuple)

    disconnect_signal = QtCore.pyqtSignal(tuple)
    bt_connect_signal = QtCore.pyqtSignal(tuple)
    exit_bt = QtCore.pyqtSignal(tuple)

    def __init__(self, event_comm, graph_comm):
        super(NICU_GUI, self).__init__()

        self.event_comm = event_comm
        self.graph_comm = graph_comm

        #Get current screen resolution
        geometry = QtGui.QDesktopWidget().screenGeometry()
        self.window = window_size_control(geometry)
        logging.info("Window mainHeight value: %s", self.window.mainHeight())
        logging.info("Window mainWidth value: %s", self.window.mainWidth())

        #connection window object
        self.connect_window = ConnectWindow(event_comm, self)
        self.setupUi()

        #initalize signals
        self.hr_update_signal.connect(self.hr_update)
        self.spo2_update_signal.connect(self.spo2_update)
        self.temp_update_signal.connect(self.temp_update)
        self.ptt_update_signal.connect(self.ptt_update)
        self.disconnect_signal.connect(self.on_bt_disconnect)
        self.bt_connect_signal.connect(self.bt_connect_func)
        self.exit_bt.connect(self.exit_button_func)

        # Add necessary events to communicator
        self.event_comm.add_event(QT_EVENT.UPDATE_HR, self.hr_update_signal.emit)
        self.event_comm.add_event(QT_EVENT.UPDATE_SPO2, self.spo2_update_signal.emit)
        self.event_comm.add_event(QT_EVENT.UPDATE_TEMP, self.temp_update_signal.emit)
        self.event_comm.add_event(QT_EVENT.UPDATE_PTT, self.ptt_update_signal.emit)

        self.event_comm.add_event(QT_EVENT.UPDATE_BUTTON_STATUS, self.connect_window.button_status_signal.emit)
        self.event_comm.add_event(QT_EVENT.UPDATE_M_LIST, self.connect_window.master_item_add_signal.emit)
        self.event_comm.add_event(QT_EVENT.UPDATE_S_LIST, self.connect_window.slave_item_add_signal.emit)
        self.event_comm.add_event(QT_EVENT.DISCONNECT, self.disconnect_signal.emit)
        self.event_comm.add_event(QT_EVENT.EXIT, self.exit_bt.emit)
        self.event_comm.add_event(QT_EVENT.BT_CONNECTED, self.bt_connect_signal.emit)

        self.graph_comm.add_graph(1, self.graph_1.updateData)
        self.graph_comm.add_graph(2, self.graph_2.updateData)
        self.graph_comm.add_graph(3, self.graph_3.updateData)

        self.event_thread = Thread(target=self.event_comm.start_event_handler, args=(PROCESS.QT,))
        self.event_thread.start()

        self.graph_event_thread = Thread(target=self.graph_comm.listen_graph_updates)
        self.graph_event_thread.start()

        # self.logger = logger()

        self.hrqueue = []
        self.spo2queue = []

        self.logging = False
        self.gui_mode = 'nicu'
        self.ca_button.hide()
        self.graph_reset_timer = 0

        self.graph_timer = QtCore.QTimer()
        self.graph_timer.timeout.connect(self.timer_func)
        self.graph_timer.start(20)
        #self.graph_timer = QtCore.QTimer()
        #self.graph_timer.timeout.connect(self.timer_func)
        #self.graph_timer.start(5000)


    def setupUi(self):
        #Main window setting
        self.setObjectName(_fromUtf8("MainWindow"))
        setmainwindowStyle(self)

        #central widget init
        self.centralwidget = QtGui.QWidget(self)
        self.centralwidget.setEnabled(True)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

        #Set title of the application
        self.title = QtGui.QLabel(self.centralwidget)
        self.title.setGeometry(QtCore.QRect(6*self.window.marginWidth(), self.window.marginHeight()*1.5,
                                      30*self.window.titlefontSize(), self.window.titlefontSize()))
        self.title.setObjectName(_fromUtf8("label"))
        self.title.setText(_translate("MainWindow", "Blood Pressure Monitoring System", None))
        setLabelStyle(self.title, self.window.titlefontSize())

        #Initalize the Graphs
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(self.window.marginWidth(), self.window.mainHeight() * .1,
                                                self.window.graphWidth(), self.window.graphHeight()))
        self.widget.setObjectName(_fromUtf8("widget"))

        self.GraphLayout = QtGui.QVBoxLayout(self.widget)
        self.GraphLayout.setSpacing(10)
        self.GraphLayout.setObjectName(_fromUtf8("verticalLayout_2"))

        # Add the graph objects
        self.graph_1 = Graph(self.widget, "Doppler", '#24a54f', 2500, False)
        self.GraphLayout.addWidget(self.graph_1.getGraph())
        self.graph_2 = Graph(self.widget, "EMG CHN 0", '#e8e5e5', 2500, False)
        self.GraphLayout.addWidget(self.graph_2.getGraph())
        self.graph_3 = Graph(self.widget, "EMG CHN 1", '#5DADE2',2500, False)
        self.GraphLayout.addWidget(self.graph_3.getGraph())

        #Initalize the BioInfo Section
        self.bioInfo = QtGui.QWidget(self.centralwidget)
        self.bioInfo.setGeometry(QtCore.QRect(self.window.bioInfoLeft(), self.window.bioInfoTop(), self.window.bioInfoWidth(), self.window.bioInfoHeight()))
        self.bioInfo.setObjectName(_fromUtf8("bioInfo")) 
        # self.bioInfo.setStyleSheet(_fromUtf8("background-color: rgb(255, 0, 0);"))

        self.BioInfoLayout = QtGui.QVBoxLayout(self.bioInfo)
        self.BioInfoLayout.setSpacing(-10)
        self.BioInfoLayout.setObjectName(_fromUtf8("BioInfoLayout"))



        # HR Value
        self.hr_text = QtGui.QLabel(self.centralwidget)
        self.hr_text.setStyleSheet("color:#24a54f ; font-size:"+str(int(self.window.bioInfonametextsize()))+"px;")
        self.hr_text.setText("<p align='center'>HR (bpm)</p>")
        self.BioInfoLayout.addWidget(self.hr_text)
        self.hr_value = QtGui.QLabel("-")
        self.hr_value.setAlignment(Qt.AlignCenter)
        self.hr_value.setStyleSheet("color:#24a54f ; font-size:"+str(int(self.window.bioInfotextsize()))+"px;padding-left:40px;")
        self.BioInfoLayout.addWidget(self.hr_value)

        # SPO2 Value
        self.spo2_text = QtGui.QLabel(self.centralwidget)
        # self.spo2_text.setGeometry(QtCore.QRect(self.window.bioInfoLeft()+120, self.window.bioInfoTop()+ self.window.bioInfoHeight()/4, self.window.bioInfonametextsize()*10, self.window.bioInfonametextsize() * 3/2))
        self.spo2_text.setStyleSheet("color:#5DADE2 ;font-size:"+str(self.window.bioInfonametextsize())+"px;")
        self.spo2_text.setText("<p align='center'> SpO<sub>2</sub> (%)</p>")
        self.BioInfoLayout.addWidget(self.spo2_text)
        self.spo2_value = QtGui.QLabel("-")
        self.spo2_value.setAlignment(Qt.AlignCenter)
        self.spo2_value.setStyleSheet("color:#5DADE2; font-size:"+str(self.window.bioInfotextsize())+"px;padding-left:40px;")
        self.BioInfoLayout.addWidget(self.spo2_value)

        # PTT
        self.ptt_text = QtGui.QLabel(self.centralwidget)
        # self.ptt_text.setGeometry(QtCore.QRect(self.window.bioInfoLeft()+120, self.window.bioInfoTop()+ self.window.bioInfoHeight()*2/4, self.window.bioInfonametextsize()*8, self.window.bioInfonametextsize()))
        self.ptt_text.setStyleSheet("color:#ffb6c1; font-size:"+str(int(self.window.bioInfonametextsize()))+"px;")
        self.ptt_text.setText("<p align='center'>PAT (ms)</p>")
        self.BioInfoLayout.addWidget(self.ptt_text)
        self.ptt_value = QtGui.QLabel("-")
        self.ptt_value.setAlignment(Qt.AlignCenter)
        self.ptt_value.setStyleSheet("color:#ffb6c1; font-size:"+str(int(self.window.bioInfotextsize()))+"px;padding-left:40px;")
        self.BioInfoLayout.addWidget(self.ptt_value)

        #Temp
        self.temptext = QtGui.QHBoxLayout()
        self.temp_text = QtGui.QLabel(self.centralwidget)
        # self.temp_text.setGeometry(QtCore.QRect(self.window.bioInfoLeft()+80, self.window.bioInfoTop() + self.window.bioInfoHeight()*3/4 + 20, self.window.bioInfonametextsize()*8, self.window.bioInfonametextsize()))
        self.temp_text.setStyleSheet("color:#C0392B ; font-size:"+str(self.window.bioInfonametextTempsize())+"px;")
        self.temp_text.setText(u"<p align='center'>T<sub>Chest</sub> (\u2103)</p>")
        self.temptext.addWidget(self.temp_text)
        self.temp_text2 = QtGui.QLabel(self.centralwidget)
        # self.temp_text2.setGeometry(QtCore.QRect(self.window.bioInfoLeft() + 100+ self.window.bioInfonametextsize()*6, self.window.bioInfoTop()+ self.window.bioInfoHeight()*3/4 + 20, self.window.bioInfonametextsize()*8, self.window.bioInfonametextsize()))
        self.temp_text2.setStyleSheet("color:#C0392B ; font-size:"+str(self.window.bioInfonametextTempsize())+"px;")
        self.temp_text2.setText(u"<p align='center'>T<sub>Limb</sub> (\u2103)</p>")
        self.temptext.addWidget(self.temp_text2)
        self.BioInfoLayout.addLayout(self.temptext)


        self.tempLayout = QtGui.QHBoxLayout()
        self.temp_value = QtGui.QLabel("-")        
        self.temp_value.setAlignment(Qt.AlignCenter)
        self.temp_value.setStyleSheet("color:#C0392B; font-size:"+str(self.window.Temptextsize())+"px;")
        self.tempLayout.addWidget(self.temp_value)
        self.temp_value2 = QtGui.QLabel("-")        
        self.temp_value2.setAlignment(Qt.AlignCenter)
        self.temp_value2.setStyleSheet("color:#C0392B; font-size:"+str(self.window.Temptextsize())+"px;")
        self.tempLayout.addWidget(self.temp_value2)
        self.BioInfoLayout.addLayout(self.tempLayout)

        #Set the text that shows the left top corner

        #Initalize the Button Section
        self.ButtonSection = QtGui.QWidget(self.centralwidget)
        self.ButtonSection.setGeometry(QtCore.QRect(self.window.bioInfoLeft(), self.window.buttonTop(), self.window.bioInfoWidth(), self.window.buttonInfoHeight()))
        self.ButtonSection.setObjectName(_fromUtf8("ButtonSection")) 
        
        self.ButtonLayout = QtGui.QVBoxLayout(self.ButtonSection)
        self.ButtonLayout.setSpacing(10)
        self.ButtonLayout.setObjectName(_fromUtf8("ButtonLayout"))

        #init connect Button
        self.connect_button = QtGui.QPushButton(self.ButtonSection)
        self.ButtonLayout.addWidget(self.connect_button)
        self.connect_button.setText(_translate("MainWindow", "Initializing", None))
        setbuttonStyle(self.connect_button, "#FFB90F")

        #init logger Button
        self.ca_button = QtGui.QPushButton(self.ButtonSection)
        self.ButtonLayout.addWidget(self.ca_button)
        self.ca_button.setText(_translate("MainWindow", "NICU Mode", None))
        setbuttonStyle(self.ca_button, "#008CBA")

        #init exit Button
        self.exit_button = QtGui.QPushButton(self.ButtonSection)
        self.ButtonLayout.addWidget(self.exit_button)
        self.exit_button.clicked.connect(self.exit_button_func)
        self.exit_button.setText(_translate("MainWindow", "Exit", None))
        setbuttonStyle(self.exit_button, "#F08080")

        self.setCentralWidget(self.centralwidget)
        #Set to full screen
        self.showFullScreen()

    @pyqtSlot(tuple)
    def bt_connect_func(self):
        self.connect_button.clicked.connect(self.ConnectButton)
        setbuttonStyle(self.connect_button, "#008CBA")
        self.connect_button.setText(_translate("MainWindow", "Bluetooth", None))

    def exit_button_func(self):
        # self.showdialog()

        self.event_comm.post_event(BLE_EVENT.EXIT)
        self.graph_comm.update_graph(-1, [])
        self.event_thread.join()
        self.graph_event_thread.join()

        sys.exit()

    def log_func(self):
        logging.debug("Logging button pressed")
        if self.logging is False:
            #set ui
            # self.log_button.setText(_translate("MainWindow", "LOGGING", None))
            # setbuttonStyle(self.log_button, "#27AE60")
            self.logging = True

            self.event_comm.post_event(BLE_EVENT.STARTLOGGING,(None,))
        else:
            # self.log_button.setText(_translate("MainWindow", "LOGGER", None))
            # setbuttonStyle(self.log_button, "#008CBA")
            self.logging = False

            self.event_comm.post_event(BLE_EVENT.STOPLOGGING,(None,))

    @pyqtSlot(float)
    def hr_update(self, args):
        if args[0] != -1:
            self.hr_value.setText(str(args[0]))
        else:
            self.hr_value.setText("-")

        '''
        self.hrqueue.append(args[0])
        logging.debug("Within hr update function")
        if len(self.hrqueue) > 4:
            logging.info("Standard deviation of hrqueue: %s", np.std(self.hrqueue))
            if np.std(self.hrqueue) < 21:
                HRaverage = sum(self.hrqueue)/len(self.hrqueue)
                self.hr_value.setText(str(HRaverage))
                # self.logger.heart_rate = HRaverage

            for i in range (3):
                self.hrqueue.pop(0)
        '''

    @pyqtSlot(tuple)
    def spo2_update(self, args):
        if args[0] != -1:
            self.spo2_value.setText(str(args[0]))
        else:
            self.spo2_value.setText("-")

        '''
        x = int(args[0])
        if x > 70:
            self.spo2queue.append(x)
            logging.debug("Within po2 update function")
            if len(self.spo2queue) > 3:
                logging.info("Standard eviation of spo2queue: %s", np.std(self.spo2queue))
                if np.std(self.spo2queue) < 3:
                    spo2average = sum(self.spo2queue)/len(self.spo2queue)
                    self.spo2_value.setText(str(int(spo2average)))
                    # self.logger.spo2 = spo2average

                self.spo2queue.pop(0)
        '''

    @pyqtSlot(tuple)
    def temp_update(self, args):
        logging.debug("Within temp_update function")
        logging.debug("Temperature value recived {}".format(args[1]))

        if args[0] == 0:
            self.temp_value.setText(str(args[1]))
        else:
            self.temp_value2.setText(str(args[1]))

    @pyqtSlot(tuple)
    def ptt_update(self, args):
        self.ptt_value.setText(str(args[0]))

    @pyqtSlot(tuple)
    def on_bt_disconnect(self, args):

        logging.info("GUI disconnect func device_type: {}".format(args[0]))

        self.clear_graph(args[0])
        self.clear_vital_signals(args[0])
        self.event_comm.post_event(QT_EVENT.UPDATE_BUTTON_STATUS, (args[0],0))

    def clear_vital_signals(self, device_type):
        if device_type == 0:
            self.hr_update([-1])
            self.temp_update([0,'-'])
        elif device_type == 1:
            self.spo2_update([-1])
            self.temp_update([1,'-'])
        self.ptt_update(['-'])
    def clear_graph(self, device_type):
        if device_type == 0:
            self.graph_1.clear_graph()
            self.graph_2.clear_graph()
            self.graph_3.clear_graph()
            self.graph_1.cur_pos = 0
            self.graph_2.cur_pos = 0
            self.graph_3.cur_pos = 0
        elif device_type == 1:
            self.graph_3.clear_graph()
            self.graph_3.cur_pos = 0
    def ConnectButton(self):
        if self.logging is True: 
            self.log_func()
        self.connect_window.clear_master()
        self.connect_window.clear_slave()
        logging.debug("Connect button")

        self.event_comm.post_event(BLE_EVENT.STARTADVERTISE, None)
        logging.debug("Passed advertising")
        self.connect_window.windowopen = True
        self.connect_window.exec_()


    def timer_func(self):

        self.graph_1.curve.setData(self.graph_1.buffer)
        self.graph_2.curve.setData(self.graph_2.buffer)
        self.graph_3.curve.setData(self.graph_3.buffer)


        self.graph_reset_timer += 1
        if self.graph_reset_timer > 100:
            if (self.graph_1.cur_pos > self.graph_1.range or self.graph_1.cur_pos < 50) and (self.graph_3.cur_pos > self.graph_3.range or self.graph_3.cur_pos < 50):
                    self.graph_1.cur_pos = 0
                    self.graph_2.cur_pos = 0
                    self.graph_3.cur_pos = 0
                    self.graph_reset_timer = 0


